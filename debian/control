Source: openmw
Rules-Requires-Root: no
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders: Bret Curtis <psi29a@gmail.com>
Build-Depends: cmake (>= 3) | cmake3,
               debhelper-compat (= 13),
               freeglut3-dev,
               libavcodec-dev,
               libavformat-dev,
               libavutil-dev,
               libboost-filesystem-dev,
               libboost-iostreams-dev,
               libboost-program-options-dev,
               libboost-thread-dev,
               libbullet-dev (>= 2.86),
               libgl-dev,
               libicu-dev,
               libluajit-5.1-dev [!ppc64el !riscv64],
               liblua5.1-0-dev [ppc64el riscv64],
               liblz4-dev,
               libmygui-dev (>= 3.2.1),
               libopenal-dev,
               libopenscenegraph-dev (>= 3.6.5),
               libqt5opengl5-dev,
               librecast-dev,
               libsdl2-dev,
               libsqlite3-dev,
               libswresample-dev,
               libswscale-dev,
               libtinyxml-dev,
               libunshield-dev,
               libyaml-cpp-dev,
Standards-Version: 4.5.1
Homepage: http://openmw.org
Vcs-Git: https://salsa.debian.org/games-team/openmw.git
Vcs-Browser: https://salsa.debian.org/games-team/openmw

Package: openmw
Architecture: any
Recommends: openmw-launcher
Depends: openmw-data (= ${source:Version}), ${misc:Depends}, ${shlibs:Depends}
Description: Open-world RPG game engine
 OpenMW is a open-source openw-world RPG game engine that started off as an
 reimplementation of the Bethesda Game Studios game engine used by The Elder
 Scrolls III: Morrowind. The project scope has grown to wanting to support more
 games and allows, with the help of the editor, for developers and content
 creators to make their own games.
 .
 To play Morrowind, the "Data Files" are required from the original game.

Package: openmw-cs
Architecture: any
Recommends: openmw
Depends: openmw-data (= ${source:Version}), ${misc:Depends}, ${shlibs:Depends}
Description: Content editor for use with OpenMW
 OpenMW-CS is a content editor for OpenMW which gives full control of the
 game's content and the ability to create new content, games and mods.
 .
 While initially similar to Bethesda's "Construction Set" for Mororwind, it is
 now fully independent and can be used to create mods, games and other content
 that do not depend on Morrowind.

Package: openmw-data
Architecture: all
Recommends: openmw
Depends: fonts-dejavu-core, fonts-ebgaramond-extra, ${misc:Depends}
Description: Resources for the OpenMW engine
 All the shaders, models, mygui xml files and extra assets necessary
 for running OpenMW.

Package: openmw-launcher
Architecture: any
Depends: openmw (>= ${source:Version}), ${misc:Depends}, ${shlibs:Depends}
Breaks: openmw-data (<< 0.47.0)
Replaces: openmw-data (<< 0.47.0)
Description: Launcher for OpenMW using the Qt-Gui-Toolkit
 A launcher for OpenMW for handling installation of games and their
 expansions, mods and configuration details.
 .
 While not necessary, it automates the first time setup and configuration of
 OpenMW.
